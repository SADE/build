# Debian (ant, git)
#
# VERSION               0.0.2

FROM frekele/ant

LABEL Description="build env for eXist-db + applications supporting various tests" Version="1.1"

RUN apt-get update && apt-get upgrade -yy
RUN apt-get install -y git graphviz ant-contrib
