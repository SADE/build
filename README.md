[![pipeline status](https://gitlab.gwdg.de/SADE/SADE/badges/develop/pipeline.svg)](https://gitlab.gwdg.de/SADE/build/commits/develop)

# Latest Builds
Besides the packages we roll out via the debian repository and our eXist
application repo (see below), we provide zip packages from the latest release
and development.
* release (build from master)
  * https://gitlab.gwdg.de/SADE/build/-/jobs/artifacts/master/download?job=build-master
* preview (build from develop)
  * https://gitlab.gwdg.de/SADE/build/-/jobs/artifacts/develop/download?job=build-develop

## Kick-start
Unzip the artifact, browse to the `bin` directory and run `startup.sh` (or
`startup.bat` if you are happy).

# SADE build targets

This repository contains build scripts for complete SADE instance of a project.

The task is to collect all sources or builds and include them in a single
artifact `build/sade-VERSION.tar.gz`. All other artifacts are available as well.

# Requirements

`ant`

# Use!

A call `ant -f generic.xml` in the root folder should result in a deployable
instance within the `./build/`. There is a tarball available but for local tests
you can simply start up the database with `./build/sade/bin/startup.sh` after build is completed.

You can download the latest artifact for the generic version [here](https://gitlab.gwdg.de/SADE/build/builds/artifacts/develop/download?job=build:develop).

# Get your own build!

To customize the instance please copy the file `generic.build.properties` to
`local.generic.build.properties` and let the property `sade.git` point to your fork
of the [SADE/SADE](https://gitlab.gwdg.de/SADE/SADE) repo, e.g.

```
sade.git = https\://gitlab.com/your-group/repo.git
```
For specific (but maybe older) cutomizations have a look on the `ant` build files
of the projects [Fontane](https://gitlab.gwdg.de/fontane-notizbuecher/build) or [Bibliothek der Neologie](https://gitlab.gwdg.de/bibliothek-der-neologie/bdn-build). They include the generic build but
override some targets and come with specific properties in the corresponding
`*.properties` files.

## Finetuning!

The file `local.generic.build.properties` allows you to set up the database to your own liking. You can configure

* how much memory eXist-db should have
* whether eXist-db's XInclude mechanism works by default
* the ports on which the database will run (HTTP/HTTPS)
* which additional SADE applications you want to install
* what description your project specific Debian package will have



# Deploy!
There are Debain packages available at the [DARIAH-DE Aptly Repo](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade/), published for
*all* distributions. They are currently untested and available for the develop
branch only. Master will follow.
