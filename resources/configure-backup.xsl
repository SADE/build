<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="scheduler">
      <scheduler>
        <xsl:apply-templates />
        <job type="system" name="check1"
          class="org.exist.storage.ConsistencyCheckTask"
          cron-trigger="0 0 23 * * ?">
          <parameter name="output" value="export"/>
          <parameter name="backup" value="yes"/>
          <parameter name="incremental" value="no"/>
          <parameter name="incremental-check" value="no"/>
          <parameter name="zip" value="yes"/>
          <parameter name="max" value="2"/>
        </job>
      </scheduler>
    </xsl:template>

</xsl:stylesheet>
