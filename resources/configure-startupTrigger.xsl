<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>

    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="triggers">
      <triggers>
        <xsl:apply-templates />
        <trigger class="org.exist.collections.triggers.XQueryStartupTrigger"/>
      </triggers>
    </xsl:template>

</xsl:stylesheet>
